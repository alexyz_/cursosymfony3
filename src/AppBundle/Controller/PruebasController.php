<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Curso;
use AppBundle\Form\CursoType;
use Symfony\Component\Validator\Constraints as assert;

class PruebasController extends Controller {

	public function indexAction(Request $request, $name, $page) {
		$productos = array(
			array("producto" => "consola", "precio" => 2),
			array("producto" => "consola 2", "precio" => 3),
			array("producto" => "consola 3", "precio" => 4),
			array("producto" => "consola 4", "precio" => 5),
			array("producto" => "consola 5", "precio" => 6)
		);

		$fruta = array("manzana" => "golden", "pera" => "rica");
		//return $this->redirect($this->request->get("router")->getContext()->getBaseUrl()."/hello-world?hola=true");
		//return $this->redirect($request->getBaseUrl()."/hello-world?hola=true");
		/* var_dump($request->query->get("hola"));
		  var_dump($request->get("hola-post"));
		  die();
		 */
		return $this->render('AppBundle:Pruebas:index.html.twig', array(
					'texto' => $name . " - " . $page,
					'productos' => $productos,
					'fruta' => $fruta
		));
	}
	
	public function createAction() {
		$curso = new Curso();
		$curso->setTitulo("Curso de Symfony 3 de Victor Robles");
		$curso->setDescripcion("Curso completo de symfony 3");
		$curso->setPrecio(80);
		
		$em = $this->getDoctrine()->getEntityManager();
		$em->persist($curso);
		$flush=$em->flush();
		
		if($flush != NULL){
			echo "El curso no se ha creado bien!!";
		}else{
			echo 'El curso se ha creado correctamente';
		}
		
		die();
	}
	
	public function readAction() {
		$em = $this->getDoctrine()->getEntityManager();
		$cursos_repo = $em->getRepository("AppBundle:Curso");
		$cursos = $cursos_repo->findAll();
		
		$cursos_ochenta = $cursos_repo->findOneByPrecio(75);
		echo $cursos_ochenta->getTitulo();
		
//		foreach ($cursos as $curso){
//			echo $curso->getTitulo()."<br/>";
//			echo $curso->getDescripcion()."<br/>";
//			echo $curso->getPrecio()."<br/><hr/>";
//		}
		
		die();
	}
	
	public function updateAction($id, $titulo, $descripcion, $precio) {
		$em = $this->getDoctrine()->getEntityManager();
		$cursos_repo = $em->getRepository("AppBundle:Curso");
		
		$curso = $cursos_repo->find($id);
		$curso->setTitulo($titulo);
		$curso->setDescripcion($descripcion);
		$curso->setPrecio($precio);
		
		$em->persist($curso);
		$flush=$em->flush();
		
		if($flush != NULL){
			echo "El curso no se ha actualizado!!";
		}else{
			echo 'El curso se ha actualizado correctamente';
		}
		
		die();
	}
	
	public function deleteAction($id) {
		$em = $this->getDoctrine()->getEntityManager();
		$cursos_repo = $em->getRepository("AppBundle:Curso");
		
		$curso = $cursos_repo->find($id);
		$em->remove($curso);
		$flush=$em->flush();
		
		if($flush != NULL){
			echo "El curso no se ha borrado!!";
		}else{
			echo 'El curso se ha borrado correctamente';
		}
		
		die();
	}
	
	public function nativeSqlAction() {
		$em = $this->getDoctrine()->getEntityManager();
                $cursos_repo = $em->getRepository("AppBundle:Curso");
//		$db = $em->getConnection();
//		
//		$query = "SELECT * FROM cursos";
//		$stmt = $db->prepare($query);
//		$params = array();
//		$stmt->execute($params);
//		
//		$cursos = $stmt->fetchAll();
		
//		$query = $em->createQuery("
//				SELECT c FROM AppBundle:Curso c
//				WHERE c.precio > :precio
//				")->setParameter("precio", "74");
//		$cursos = $query->getResult();
                
//                $query = $cursos_repo->createQueryBuilder("c")
//                        ->where("c.precio > :precio")
//                        ->setParameter("precio", "74")
//                        ->getQuery();
//                $cursos = $query->getResult();
                
                $cursos = $cursos_repo->getCursos();
		
		foreach($cursos as $curso){
			echo $curso->getTitulo()."<br/>";
		}
		
		die();
	}
        
        public function formAction(Request $request) {
            $curso = new Curso();
            $form = $this->createForm(CursoType::class, $curso);
            
            $form->handleRequest($request);
            
            if($form->isValid()){
                $status = "Formulario valido";
                $data = array(
                    "titulo" => $form->get("titulo")->getData(),
                    "descripcion" => $form->get("descripcion")->getData(),
                    "precio" => $form->get("precio")->getData()
                );
            }else{
                $status = null;
                $data = null;
            }
            
            return $this->render('AppBundle:Pruebas:form.html.twig', array(
                'form' => $form->createView(),
                'status' => $status,
                'data' => $data
            ));
        }
        
        public function validarEmailAction($email) {
            $emailConstraint = new assert\Email();
            $emailConstraint->message = "Pasame un buen correo";
            
            $error = $this->get("validator")->validate(
                    $email,
                    $emailConstraint
                    );
            if(count($error) == 0){
                echo '<h1>Correo valido!!!</h1>';
            }else{
                echo $error[0]->getMessage();
            }
            
            die();
        }

}
