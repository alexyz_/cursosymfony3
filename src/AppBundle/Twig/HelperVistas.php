<?php
namespace AppBundle\Twig;

class HelperVistas extends \Twig_Extension{
	public function getFunctions() {
		return array(
			'generateTable' => new \Twig_Function_Method($this, 'generateTable')
		);
	}
	
	public function generateTable($resultset) {
		$table="<table class='table' border=1>";
		for($i=0; $i< count($resultset); $i++){
			$table.="<tr>";
			for($j=0; $j< count($resultset[$i]); $j++){
				$resultset_values= array_values($resultset[$i]);
				$table.="<td>".$resultset_values[$j]."</td>";
			}
			$table.="</tr>";
		}
		$table.="</table>";
		return $table;
	}
	
	public function getName() {
		return "app_bundle";
	}

}